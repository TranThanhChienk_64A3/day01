create database QLSV;

use QLSV;

create table DMKHOA(
MaKH		varchar(6) 			primary key,
TenKhoa		varchar(30)
)
;

create table SINHVIEN(
MaSV 		varchar(6) 			primary key,
HoSV		varchar(30) 		not null,
TenSv		varchar(15) 		not null,
GioiTinh	Char(1) 			not null,
NgaySinh	DateTime 			not null,
NoiSinh		varchar(50) 		not null,
DiaChi		varchar(50) 		not null,
MaKH 		varchar(6) 			not null,
HocBong		int 				not null
)
;
alter table SINHVIEN
add constraint FK_MaKH_sv foreign key(MaKH) references DMKHOA(MaKH);

insert into DMKHOA(MaKH,TenKhoa)
values 	('IT', 		'Cong nghe thong tin'),
		('GEO', 	'Dia ly'),
		('BIO',		'Sinh hoc'),
        ('CHEM',	'Hoa');

insert into SINHVIEN(MaSV,HoSV,TenSV,GioiTinh,NgaySinh,NoiSinh,DiaChi,MaKH,HocBong)
values 	('000111',	'tran',		'thanh chien',	'm',	'2001-01-01',	'Ha Noi',		'123 Hai Ba Trung',	'IT',	'1243'),
		('000222',	'nguyen',	'hung',			'm',	'2002-02-04',	'Hung Yen', 	'31 Truong Chinh',	'GEO',	'1000'),
        ('000333',	'chu',		'ngoc hoa',		'f',	'2001-05-12',	'Vinh Phuc',	'4 Ngoc Hoa',		'IT',	'10000');
        